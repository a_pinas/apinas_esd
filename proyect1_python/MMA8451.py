#!/usr/bin/env python

import pigpio
import sys
import time

i2c_bus = 1
i2c_address = 0x1C
i2c_flags = 0

pi = pigpio.pi()
if not pi.connected:
    print("Error: Exiting")
    exit()

handle = pi.i2c_open(i2c_bus, i2c_address, i2c_flags)

pi.i2c_write_byte_data(handle, 0x0E, 0x01)	#Resolution set to 4g BEFORE entering active mode
pi.i2c_write_byte_data(handle, 0x2A, 0x01)	#Entering active mode

time.sleep(1)

while 1:
    (b, d) = pi.i2c_read_i2c_block_data(handle, 0x01, 6)
    
    if b>= 0:
        
        x = ((d[0] << 8) | d[1] ) >> 2
        if x > 8191: 		#if the number is negative
            x = x - 16384
        x = x/2048
        
        y = ((d[2] << 8) | d[3]) >> 2
        if y > 8191: 		#if the number is negative
            y = y - 16384
        y = y/2048
        
        z = ((d[4] << 8) | d[5]) >> 2
        if z > 8191: 		#if the number is negative
            z = z - 16384
        z = z/2048
        
        print("x-axis = %.2f\n" % x)
        print("y-axis = %.2f\n" % y)
        print("z-axis = %.2f\n"% z)
        time.sleep(3)
        
    else:
        print("ERROR READING I2C")
        
pi.i2c_close(handle)
pi.stop()
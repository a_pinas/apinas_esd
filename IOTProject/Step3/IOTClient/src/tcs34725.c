/*
 * main.c
 *
 *  Created on: Mar 29, 2021
 *      Author: Alejandro Piñas, Alvaro Cañibano
 */

#include <unistd.h>
#include <stdio.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>
#include <sys/types.h>
#include <stdbool.h>
#include "tcs34725.h"

#define ADDR 0x29

float getRGB(uint16_t colour, uint16_t clear); //Gets RGB value

uint16_t clear, red, green, blue;
float r, g, b;

static int fd;								//I2C file descriptorç



int initializeTCS34725(int i2c_fd){

	/*Getting file descriptor*/
	fd = i2c_fd;

	/*Initialization of the I2C*/
	printf("Initializing TCS34725...\n");

	struct i2c_rdwr_ioctl_data cfg_packets;
	struct i2c_msg cfg_messages[4];

	int w_len = 2;

	/*ATIME Register configuration,	 */
	unsigned char RGBC_timing_cfg[w_len];

	RGBC_timing_cfg[0] = 0x81; //MSB selects the command register.
							   //5 LSB select the target register we are going to write to.

	RGBC_timing_cfg[1] = 0x00;	//Integration time
	cfg_messages[0].addr = ADDR;
	cfg_messages[0].flags = 0; // 0 => Write
	cfg_messages[0].len = w_len;
	cfg_messages[0].buf = RGBC_timing_cfg;

	/*CONTROL register configuration*/
	//Might not be needed, its set to 0x00 by default
	unsigned char CONTROL_cfg[w_len];
	CONTROL_cfg[0] = 0x8F; //5 LSB are the register address
	CONTROL_cfg[1] = 0x00; //Gain X1
	cfg_messages[1].addr = ADDR;
	cfg_messages[1].flags = 0; // 0 => Write
	cfg_messages[1].len = w_len;
	cfg_messages[1].buf = CONTROL_cfg;

	/*ENABLE register configuration*/
	unsigned char ENABLE_cfg[w_len];
	ENABLE_cfg[0] = 0x80; //5 LSB are the register address
	ENABLE_cfg[1] = 0x03; //RGBC enable, Power ON
	cfg_messages[2].addr = ADDR;
	cfg_messages[2].flags = 0; // 0 => Write
	cfg_messages[2].len = w_len;
	cfg_messages[2].buf = ENABLE_cfg;

	/*Setting CDATAL in the command register*/
	unsigned char write_byte = 0xB4; //Auto-increment. Reading starts at CDATAL
	cfg_messages[3].addr = ADDR;
	cfg_messages[3].flags = 0; // 0 => Write
	cfg_messages[3].len = 1;
	cfg_messages[3].buf = &write_byte;

	cfg_packets.msgs = cfg_messages;
	cfg_packets.nmsgs = 4;

	/*Sending configuration packet*/
	if( ioctl(fd, I2C_RDWR, &cfg_packets) < 0 ){
		printf("[RPI,TCS34725] ioctl error\n");
		return -1;
	}
	printf("[RPI,TCS35725] Initialized\n");

	return 0;
}

int readTCS34725(uint8_t *r_out, uint8_t *g_out, uint8_t *b_out){		//Parameters to be defined

	struct i2c_rdwr_ioctl_data packet;	//Packet to read data from the sensor
	struct i2c_msg message[1];


	unsigned char read_bytes_tcs[8];	//Reception buffer

	/*Assambling data packet*/
	int r_len = 8;						//We are going to read 8 bytes
	message[0].addr = ADDR;
	message[0].flags = I2C_M_RD;
	message[0].len = r_len;
	message[0].buf = read_bytes_tcs;

	packet.msgs = message;
	packet.nmsgs = 1;

	if(ioctl(fd, I2C_RDWR, &packet) < 0){
		printf("[TCS34725]ioctl error");
		return -1;
	}

	clear = (read_bytes_tcs[1] << 8) | read_bytes_tcs[0];
	red = (read_bytes_tcs[3] << 8) | read_bytes_tcs[2];
	green = (read_bytes_tcs[5] << 8) | read_bytes_tcs[4];
	blue = (read_bytes_tcs[7] << 8) | read_bytes_tcs[6];

	r =  getRGB(red, clear);
	g =  getRGB(green, clear);
	b =  getRGB(blue, clear);

	printf("[RPI] R: %d G: %d B: %d \n", (int)r, (int)g, (int)b);

	*r_out = (int) r;
	*g_out = (int) g;
	*b_out = (int) b;

	return 0;
}

int stopTCS34725(void){

	printf("[RPI,TCS34725]Turning off...\n");

	/*Turning off the sensor*/
	int w_len = 2;
	unsigned char ENABLE_cfg[w_len];
    struct i2c_rdwr_ioctl_data final_packet;
	struct i2c_msg final_msg[1];
	ENABLE_cfg[0] = 0x80;
	ENABLE_cfg[1] = 0x00;
	final_msg[0].addr = ADDR;
	final_msg[0].flags = 0;
	final_msg[0].len = w_len;
	final_msg[0].buf = ENABLE_cfg;

	final_packet.msgs = final_msg;
	final_packet.nmsgs = 1;


	if( ioctl(fd, I2C_RDWR, &final_packet) < 0 ){
		printf("[RPI,TCS34725]ioctl error\n");
		return -1;
	}


	return 0;
}



float getRGB (uint16_t colour, uint16_t clear){

	  float value;
	  if (clear == 0) {
	    return 0;
	  }

	  value = (float)colour / clear * 255.0;
	  return value;

}





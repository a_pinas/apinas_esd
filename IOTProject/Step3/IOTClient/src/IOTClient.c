/*
 *  main.c
 *  IOTClient source
 *  Created on: Apr 26, 2021
 *      Author: rpi
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <stdio.h>
#include <stdbool.h>
#include "tcs34725.h"
#include "mma8451.h"
#include "rgbLED.h"

/*Opens i2c file descriptor and initializes devices*/
int initializeDevices(void);
/*Stops devices*/
void stopDevices(void);

#define MAX_SAMPLES 10
#define MAX_CMD 16

/*To exchange data*/

struct data {
	uint8_t n_samples;
	uint8_t r[MAX_SAMPLES];
	uint8_t g[MAX_SAMPLES];
	uint8_t b[MAX_SAMPLES];
	float x[MAX_SAMPLES];
	float y[MAX_SAMPLES];
	float z[MAX_SAMPLES];
};

/*To exchange commands*/


struct command {

	char command[MAX_CMD];

};

int sample_rate = 1; //1 second by default
uint8_t samples = 10;    //10 samples by default
bool run = true;

int main(int argc, char *argv[]) {

	if (argc < 3 || argc > 5) {
		printf(
				"Usage: %s <host_address>  <port> [sample_rate_s] [samples_n] \n",
				argv[0]);
		printf(
				"[samples_n] is the number of samples to send to the server, range from 1 to 10 samples\n ");
		return 0;
	} else if (argc == 5) {
		sample_rate = atoi(argv[3]);
		samples = atoi(argv[4]);
	}


	printf("Starting communication with %s on port %s.\n", argv[1], argv[2]);
	printf("Sample rate: %d seconds, Number of samples: %d\n", sample_rate,
			samples);


	/*Socket configuration*/
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0); //AF_INET => IPv4 Internet protocols
													//SOCK_DGRAM => Connectionless protocol
	if (socket_fd < 0) {
		printf("Error creating socket.\n");
		return 0;
	} else {
		printf("Socket created.\n");
	}

	/*Server information*/
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET; 					//IPv4
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);  	//Server IP, String to IP address
	server_addr.sin_port = htons(atoi(argv[2])); 		//Server Port, String to Integer,
														//Integer to network byte order


	int server_size = sizeof(server_addr);

	/*Build packets*/
	char out_cmd_buffer[16];
	struct command *out_command_packet = (struct command*) out_cmd_buffer;

	char in_cmd_buffer[16];
	struct command *in_command_packet = (struct command*) in_cmd_buffer;

	char buffer[256];
	struct data *out_packet = (struct data*) buffer;

	/*Send hello*/
	sprintf(out_command_packet->command, "hello");

	if (sendto(socket_fd, out_command_packet, sizeof(struct command), 0,
					(struct sockaddr*) &server_addr, server_size) < 0){
		printf("Error sending message.\n");
		close(socket_fd);
		return -1;
	} else {
		printf("Sent hello to the server.\n");

		/*Wait to receive message*/

		if (recvfrom(socket_fd, in_command_packet, sizeof(struct command), 0,
				(struct sockaddr*) &server_addr, &server_size) < 0) {
			printf("Error receiving.\n");
			close(socket_fd);
			return -1;
		} else {
			/*if the message is a hello, we can continue*/
			if(strcmp(in_command_packet->command, "hello") == 0){
				printf("[Server] %s\n", in_command_packet->command);
			} else{
				printf("Received wrong message.\n");
				close(socket_fd);
				return -1;
			}
		}



	}

	/*Initializing devices*/
	int i2c_fd = initializeDevices();


	uint8_t r, g, b;
	float x, y, z;

	/*Number of samples*/



	/*loop*/
	while (true) {

		int i = 0;
		do {
			/*If a command is received, process it*/
			if(recvfrom(socket_fd, in_command_packet,
					sizeof(struct command), MSG_DONTWAIT, (struct sockaddr*) &server_addr,
					&server_size) >= 0){
				printf("[Server]: %s\n", in_command_packet->command);

				/*if command stop, stop devices and free resources*/
				if (strcmp(in_command_packet->command,"stop") == 0){
					stopDevices();
					close(socket_fd);
					close(i2c_fd);
					return 0;
				}
			}
			readMMA8451(&x, &y, &z);
			readTCS34725(&r, &g, &b);

			out_packet->r[i] = r;
			out_packet->g[i] = g;
			out_packet->b[i] = b;
			out_packet->x[i] = x;
			out_packet->y[i] = y;
			out_packet->z[i] = z;
			i++;
			sleep(sample_rate);
		} while (i < samples);

		/*Sending Message*/
		printf("Sending data to the server.\n");
		out_packet->n_samples = samples;
		if (sendto(socket_fd, out_packet, sizeof(struct data), 0,
				(struct sockaddr*) &server_addr, server_size) < 0) {
			printf("Error sending message.\n");
			return 0;
		} else {
			printf("Message sent succesfully.\n");
		}

		/*Receiving message*/

		if (recvfrom(socket_fd, in_command_packet, sizeof(struct command), 0,
				(struct sockaddr*) &server_addr, &server_size) < 0) {
			printf("Error receiving.\n");
			close(socket_fd);
			close(i2c_fd);
			return 0;
			return -1;
		} else {
			if(strcmp(in_command_packet->command, "OK") == 0){
				printf("[Server] %s\n", in_command_packet->command);
			}
			else{
				printf("No ACK.\n");
				printf("[Server] %s\n", in_command_packet->command);
				close(socket_fd);
				close(i2c_fd);
				return -1;
			}
		}


	}

}

int initializeDevices(void) {

	int i2c_fd = open("/dev/i2c-1", O_RDWR);
	if (i2c_fd < 0) {
		printf("Error opening i2c-1");
		return -1;
	}

	initializeMMA8451(i2c_fd);
	initializeTCS34725(i2c_fd);

	sleep(1);
	return i2c_fd;
}

void stopDevices(void){
	stopTCS34725();
	stopMMA8451();
	printf("Devices stopped.\n");
}

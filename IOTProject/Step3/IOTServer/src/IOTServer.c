/*
 ============================================================================
 Name        : IOTServer.c
 Author      : Alvaro y Alejandro
 Version     :
 Copyright   : Your copyright notice
 Description : UDP Server to run in the Ubuntu Machine

 Steps:
 Create UPD Socket
 Bind socket to server address
 Wait until datagram packet arrives from client
 Process datagram
 Send reply to client
 Close socket
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <stdbool.h>
#include <math.h>

#define MAX_SAMPLES 10
#define MAX_CMD 16

struct data {
	uint8_t n_samples;
	uint8_t r[MAX_SAMPLES];
	uint8_t g[MAX_SAMPLES];
	uint8_t b[MAX_SAMPLES];
	float x[MAX_SAMPLES];
	float y[MAX_SAMPLES];
	float z[MAX_SAMPLES];
};

struct command {

	char command[MAX_CMD];
//int rgb[3]; Solo tiene sentido si implementamos comandos

};

void SIGINT_Handler(int sig);
float calculate_mean(uint8_t samples, float colour[]);
float calculate_max(uint8_t samples, float colour[]);
float calculate_min(uint8_t samples, float colour[]);
double calculate_std(uint8_t samples, float colour[], float mean);

static int run = true;

float mean_r, mean_g, mean_b;

uint8_t samples;

int main(void) {

	/*SIGINT Handler*/
	signal(SIGINT, SIGINT_Handler);

	int status;
	struct sockaddr_in server_addr, client_addr;
	int client_struct_length = sizeof(client_addr);

	/*Input packet*/
	char buffer[256];
	struct data *in_packet = (struct data*) buffer;

	/*Command packet*/
	char in_cmd_buffer[16];
	struct command *in_command_packet = (struct command*) in_cmd_buffer;

	/*out Command packet*/
	char out_cmd_buffer[16];
	struct command *out_command_packet = (struct command*) out_cmd_buffer;

	/*Create UDP Socket*/
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (socket_fd < 0) {
		printf("Error while creating socket\n");
		return -1;
	}
	printf("Socket created successfully\n");

	   memset(&server_addr, 0, sizeof(server_addr));
	    memset(&client_addr, 0, sizeof(client_addr));

	/*Server info*/
	server_addr.sin_family = AF_INET;			//Internet domain
	server_addr.sin_port = 0;  					//Use any available port
	server_addr.sin_addr.s_addr = INADDR_ANY;	//Server addr

	/*Bind socket*/
	if (bind(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr))
			< 0) {
		printf("Couldn't bind to the port\n");
		return -1;
	}
	printf("Done with binding\n");
	int len = sizeof(server_addr);

	/*Getting port used*/
	status = getsockname(socket_fd, (struct sockaddr*) &server_addr, &len);
	if (status < 0) {
		printf("Error getting socket name.\n");
	}

	printf("Port assigned: %d\n", ntohs(server_addr.sin_port));
	printf("Listening for incoming messages...\n\n");

	/*Wait to receive message*/


	if (recvfrom(socket_fd, in_command_packet, sizeof(struct command), 0 ,
			(struct sockaddr*) &client_addr, &client_struct_length) < 0) {
		printf("Error receiving packet.\n");
		close(socket_fd);
		return -1;
	} else {
		printf("[RPI] %s\n", in_command_packet->command);

		/*If the message received is hello, send it back*/
		if (strcmp(in_command_packet->command, "hello") == 0) {
			sprintf(out_command_packet->command, "hello");
			if (sendto(socket_fd, out_command_packet, sizeof(struct command), 0,
					(struct sockaddr*) &client_addr, client_struct_length)
					< 0) {
				printf("Error sending message\n");
				close(socket_fd);
				return -1;
			} else {
				printf("Sent hello to the RPI. \n");
			}

		} else {
			printf("Received wrong messange.\n");
			close(socket_fd);
			return -1;
		}

	}

	int received;
	/*Loop*/
	while (true) {

		do {
			received = recvfrom(socket_fd, in_packet, sizeof(struct data),
			MSG_DONTWAIT, (struct sockaddr*) &client_addr,
					&client_struct_length);

			/*If SIGINT is received, close the file descriptor and end the program*/
			if (run == 0) {
				/*Send stop message to the client*/
				sprintf(out_command_packet->command, "stop");
				if (sendto(socket_fd, out_command_packet, sizeof(struct command), 0,
						(struct sockaddr*) &client_addr, client_struct_length)
						< 0) {
					printf("Error sending message.\n");
				}

				close(socket_fd);
				return 0;
			}
		} while (received < 0);

		samples = in_packet->n_samples;


		/*
		for (int i = 0; i < samples; i++) {
			printf("R: %d G: %d B: %d\n", in_packet->r[i], in_packet->g[i],
					in_packet->b[i]);
			printf("X: %.3f Y: %.3f Z: %.3f\n", in_packet->x[i],
					in_packet->y[i], in_packet->z[i]);
		}
		*/

		float r[MAX_SAMPLES];
		float g[MAX_SAMPLES];
		float b[MAX_SAMPLES];


		for(int i = 0; i < samples; ++i) {
		    r[i] = (float)in_packet->r[i];
		    g[i] = (float)in_packet->g[i];
		    b[i] = (float)in_packet->b[i];
		}

		mean_r = calculate_mean(samples, r);
		mean_g = calculate_mean(samples, g);
		mean_b = calculate_mean(samples, b);
		printf("Results: \n\n");
		/*Calculate Mean, Max, Min and Standar deviation*/
		printf("Mean--> R: %.3f G: %.3f B: %.3f\n",
				mean_r,
				mean_g,
				mean_b);
		printf("Mean--> X: %.3f Y: %.3f Z: %.3f\n",
				calculate_mean(samples, in_packet->x),
				calculate_mean(samples, in_packet->y),
				calculate_mean(samples, in_packet->z));
		printf("Max--> R: %.3f G: %.3f B: %.3f\n",
				calculate_max(samples, r),
				calculate_max(samples, g),
				calculate_max(samples, b));
		printf("Max--> X: %.3f Y: %.3f Z: %.3f\n",
				calculate_max(samples, in_packet->x),
				calculate_max(samples, in_packet->y),
				calculate_max(samples, in_packet->z));
		printf("Min--> R: %.3f G: %.3f B: %.3f\n",
				calculate_min(samples, r),
				calculate_min(samples, g),
				calculate_min(samples, b));
		printf("Min--> X: %.3f Y: %.3f Z: %.3f\n",
				calculate_min(samples, in_packet->x),
				calculate_min(samples, in_packet->y),
				calculate_min(samples, in_packet->z));
		printf("Standard Deviation--> R: %.3f G: %.3f B: %.3f\n",
				calculate_std(samples, r,
						mean_b),
				calculate_std(samples, g,
						mean_g),
				calculate_std(samples, b,
						mean_b));
		printf("Standard Deviation--> X: %g Y: %g Z: %g\n\n",
				calculate_std(samples, in_packet->x,
						calculate_mean(samples, in_packet->x)),
				calculate_std(samples, in_packet->y,
						calculate_mean(samples, in_packet->y)),
				calculate_std(samples, in_packet->z,
						calculate_mean(samples, in_packet->z)));


		sprintf(out_command_packet->command,"OK");
		if (sendto(socket_fd, out_command_packet, sizeof(struct command), 0,
				(struct sockaddr*) &client_addr, client_struct_length) < 0) {
			printf("Error sending message.\n");
		}
	}
}

void SIGINT_Handler(int sig) {
	printf("[Server] SIGINT received.\n");
	run = false;
}

float calculate_mean(uint8_t samples, float value[]) {
	float result;
	float mean = 0;
	for (int i = 0; i < samples; i++) {
		mean = mean + value[i];
	}
	result = mean / samples;
	return result;
}

float calculate_max(uint8_t samples, float value[]) {
	float max = value[0];
	for (int i = 0; i < samples; i++) {
		if (max <= value[i]) {
			max = value[i];
		}
	}
	return max;
}

float calculate_min(uint8_t samples, float value[]) {
	float min = value[0];
	for (int i = 0; i < samples; i++) {
		if (min >= value[i]) {
			min = value[i];
		}
	}
	return min;
}

double calculate_std(uint8_t samples, float value[], float mean) {
	float std_dev = 0;
	for (int i = 0; i < samples; i++) {
		std_dev += pow((value[i] - mean), 2);
	}
	std_dev = sqrt(std_dev / samples);
	return std_dev;
}


/*
 *  main.c
 *  IOTClient source
 *  Created on: Apr 26, 2021
 *      Author: rpi
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAXSIZE 1024

int main (int argc, char *argv[]){

	if(argc != 3){
		printf("Usage: %s <host_address>  <port>", argv[0]);
		return 0;
	}


	uint16_t port = htons(atoi(argv[2])); //String=>Integer=>Network order
											//atoi obsolete change for strtol?ç

	printf("Starting communication with %s on port %s.\n", argv[1], argv[2]);

	/*Socket configuration*/
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0); //AF_INET => IPv4 Internet protocols
	if (socket_fd < 0){								//SOCK_DGRAM => Connectionless protocol
		printf("Error creating socket.\n");
		exit(1);
	}else{
		printf("Socket created.\n");
	}


	/*Server information*/
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET; 					//IPv4
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);  	//Server IP
	server_addr.sin_port = port; 						//Server Port

	/*Client information*/
	struct sockaddr_in client_addr;
	client_addr.sin_family = AF_INET;			//IPv4
	client_addr.sin_addr.s_addr = INADDR_ANY;	//Any IP
	client_addr.sin_port = 0; 					//The system will choose the port

	/*Sending Message*/
	char out_buf[] = "Hello Server";
	int out_buf_size = strlen(out_buf) + 1;

	printf("Sending Hello Server.\n");

	int status = sendto(socket_fd, out_buf, out_buf_size, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (status < 0){
		printf("Error sending message.\n");
		exit(2);
	} else {
		printf("Message sent succesfully.\n");
	}


	/*Receiving message*/
	char in_buf[MAXSIZE];
	int client_size = sizeof(client_addr);
	status = recvfrom(socket_fd, in_buf, MAXSIZE , 0, (struct sockaddr *)&client_addr, &client_size);
	if (status < 0){
		printf("Error receiving.\n");
		exit(3);
	} else {
		printf("Message received from the server.\n");
	}
	printf("[Server] %s\n", in_buf);

	close(socket_fd);
}

/*
 ============================================================================
 Name        : UDP-Server.c
 Author      : Alvaro y Alejandro
 Version     :
 Copyright   : Your copyright notice
 Description : UDP Server to run in the Ubuntu Machine

 Pasos:
 Create UPD Socket
 Bind socket to server address
 Wait until datagram packet arrives from client
 Process datagram
 Send reply to client
 Close socket
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define MAXSIZE 1024

int main(void) {

	int status;
	struct sockaddr_in server_addr, client_addr;

	// Create UDP socket:
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);

	if(socket_fd < 0){
		printf("Error while creating socket\n");
	    return -1;
	}
	printf("Socket created successfully\n");


	// Server info
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = 0;  					//Use any available port
	server_addr.sin_addr.s_addr = INADDR_ANY;	//Any address in the net

	// Bind socket
	if(bind(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
		printf("Couldn't bind to the port\n");
	    return -1;
	}
	printf("Done with binding\n");
	int len = sizeof(server_addr);
	status = getsockname(socket_fd, (struct sockaddr *) &server_addr, &len);
	if (status < 0){
		printf("Error getting socket name.\n");
	}

	printf("Port assigned: %d\n", ntohs(server_addr.sin_port));
	printf("Listening for incoming messages...\n\n");


	//while(client_message!= "apagar"){//mientras no se envíe apagar

		// Receive client's message:
		char client_message[MAXSIZE];
		int client_struct_length = sizeof(client_addr);
		if (recvfrom(socket_fd, client_message, sizeof(client_message) , 0 , (struct sockaddr*)&client_addr, &client_struct_length) < 0){
				printf("Couldn't receive\n");
				return -1;
		 }

		printf("Msg from client: %s\n", client_message);

		if (strcmp(client_message,"Hello Server") == 0) {
			char outbuf[]= "Hello RPI";
			if (sendto(socket_fd, outbuf, strlen(outbuf), 0, (struct sockaddr*)&client_addr, client_struct_length) < 0){
						printf("Can't send\n");
						return -1;
					}
		}else{
			char outbuf2[] = "Wrong message";
			if (sendto(socket_fd, outbuf2, strlen(outbuf2), 0, (struct sockaddr*)&client_addr, client_struct_length) < 0){
						printf("Can't send\n");
						return -1;
					}
		}

		/*printf("Received message from IP: %s and port: %i\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

		 printf("Msg from client: %s\n", client_message);

		 // Respond to client:
		 strcpy(server_message, client_message);

		 if (sendto(newSocket, server_message, strlen(server_message), 0, (struct sockaddr*)&client_addr, client_struct_length) < 0){
			 printf("Can't send\n");
				return -1;
		 }*/
	 //}

	 // Close the socket:
	 close(socket_fd);

	 return 0;
}


/*
 ============================================================================
 Name        : IOTServer.c
 Author      : Alvaro y Alejandro
 Version     :
 Copyright   : Your copyright notice
 Description : UDP Server to run in the Ubuntu Machine

 Steps:
 Create UPD Socket
 Bind socket to server address
 Wait until datagram packet arrives from client
 Process datagram
 Send reply to client
 Close socket
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <stdbool.h>
#include <math.h>

struct packet {
	int n_samples;
	int r[10];
	int g[10];
	int b[10];
	float x[10];
	float y[10];
	float z[10];
};

void SIGINT_Handler(int sig);
int calculate(struct packet *package);

#define MAXSIZE 1024

int run = true;

int main(void) {

	/*SIGINT Handler*/
	signal(SIGINT, SIGINT_Handler);

	int status;
	struct sockaddr_in server_addr, client_addr;
	int client_struct_length = sizeof(client_addr);

	// Create UDP socket:
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (socket_fd < 0) {
		printf("Error while creating socket\n");
		return -1;
	}
	printf("Socket created successfully\n");

	/*Server info*/
	server_addr.sin_family = AF_INET;			//Internet domain
	server_addr.sin_port = 0;  					//Use any available port
	server_addr.sin_addr.s_addr = INADDR_ANY;	//Server addr

	/*Bind socket*/
	if (bind(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr))
			< 0) {
		printf("Couldn't bind to the port\n");
		return -1;
	}
	printf("Done with binding\n");
	int len = sizeof(server_addr);

	/*Getting port used*/
	status = getsockname(socket_fd, (struct sockaddr*) &server_addr, &len);
	if (status < 0) {
		printf("Error getting socket name.\n");
	}

	printf("Port assigned: %d\n", ntohs(server_addr.sin_port));
	printf("Listening for incoming messages...\n\n");

	char buffer[256];
	struct packet *in_packet = (struct packet*) buffer;

	int received;

	/*Loop*/
	while (run) {

		do{
			received = recvfrom(socket_fd, in_packet, sizeof(struct packet),
			MSG_DONTWAIT, (struct sockaddr*) &client_addr,
					&client_struct_length);

			/*If SIGINT is received, close the file descriptor and end the program*/
			if(!run){
				close(socket_fd);
				return 0;
			}
		} while (received == -1);

		int samples = in_packet->n_samples;

		for (int i = 0; i < samples; i++) {
			printf("R: %d G: %d B: %d\n", in_packet->r[i], in_packet->g[i],
					in_packet->b[i]);
			printf("X: %.3f Y: %.3f Z: %.3f\n", in_packet->x[i],
					in_packet->y[i], in_packet->z[i]);
		}

		if (calculate(in_packet) < 0) {
			char outbuf[] = "ERROR";
			if (sendto(socket_fd, outbuf, strlen(outbuf), 0,
					(struct sockaddr*) &client_addr, client_struct_length)
					< 0) {
				printf("[Server] Error sending message\n");
				return -1;
			}
		} else {
			char outbuf2[] = "OK";
			if (sendto(socket_fd, outbuf2, strlen(outbuf2), 0,
					(struct sockaddr*) &client_addr, client_struct_length)
					< 0) {
				printf("[Server] Error sending message\n");
				return -1;
			}
		}

	}

	close(socket_fd);

	return 0;
}

void SIGINT_Handler(int sig) {
	printf("[Server] SIGINT received.\n");
	run = false;
}

int calculate(struct packet *package) {	//Function that calculates the mean, max and min of the data collected

	float red_mean;
	float green_mean;
	float blue_mean;
	float x_mean;
	float y_mean;
	float z_mean;
	float red_max = package->r[0];
	float red_min = package->r[0];
	float green_max = package->g[0];
	float green_min = package->g[0];
	float blue_max = package->b[0];
	float blue_min = package->b[0];
	float x_max = package->x[0];
	float x_min = package->x[0];
	float y_max = package->y[0];
	float y_min = package->y[0];
	float z_max = package->z[0];
	float z_min = package->z[0];
	float sd_red = 0;
	float sd_green = 0;
	float sd_blue = 0;
	double sd_x = 0;
	double sd_y = 0;
	double sd_z = 0;

	for (int i = 0; i < 10; i++) {

		//Max
		if (red_max <= package->r[i]) {
			red_max = package->r[i];
		}
		if (blue_max <= package->b[i]) {
			blue_max = package->b[i];
		}
		if (green_max <= package->g[i]) {
			green_max = package->g[i];
		}
		if (x_max <= package->x[i]) {
			x_max = package->x[i];
		}
		if (y_max <= package->y[i]) {
			y_max = package->y[i];
		}
		if (z_max <= package->z[i]) {
			z_max = package->z[i];
		}

		//Min
		if (red_min >= package->r[i]) {
			red_min = package->r[i];
		}
		if (blue_min >= package->b[i]) {
			blue_min = package->b[i];
		}
		if (green_min >= package->g[i]) {
			green_min = package->g[i];
		}
		if (x_min >= package->x[i]) {
			x_min = package->x[i];
		}
		if (y_min >= package->y[i]) {
			y_min = package->y[i];
		}
		if (z_min >= package->z[i]) {
			z_min = package->z[i];
		}

		//Mean
		red_mean = red_mean + package->r[i];
		green_mean = green_mean + package->g[i];
		blue_mean = blue_mean + package->b[i];
		x_mean = x_mean + package->x[i];
		y_mean = y_mean + package->y[i];
		z_mean = z_mean + package->z[i];

	}
//Mean
	red_mean = red_mean / 10;
	blue_mean = blue_mean / 10;
	green_mean = green_mean / 10;
	x_mean = x_mean / 10;
	y_mean = y_mean / 10;
	z_mean = z_mean / 10;

//Standard deviation
	for (int i = 0; i < 10; i++) {
		sd_red += abs(package->r[i] - red_mean) * abs(package->r[i] - red_mean);
		sd_green += abs(package->g[i] - green_mean)
				* abs(package->g[i] - green_mean);
		sd_blue += abs(package->b[i] - blue_mean)
				* abs(package->b[i] - blue_mean);
		sd_x += abs(package->x[i] - x_mean) * abs(package->x[i] - x_mean);
		sd_y += abs(package->y[i] - y_mean) * abs(package->y[i] - y_mean);
		sd_z += abs(package->z[i] - z_mean) * abs(package->z[i] - z_mean);
	}
//Standard deviation
	sd_red = sqrt(sd_red / 10);
	sd_green = sqrt(sd_green / 10);
	sd_blue = sqrt(sd_blue / 10);
	sd_x = sqrt(sd_x / 10);
	sd_y = sqrt(sd_y / 10);
	sd_z = sqrt(sd_z / 10);

	printf("Mean--> R: %.3f G: %.3f B: %.3f\n", red_mean, green_mean,
			blue_mean);
	printf("Mean--> X: %.3f Y: %.3f Z: %.3f\n", x_mean, y_mean, z_mean);
	printf("Max--> R: %.3f G: %.3f B: %.3f\n", red_max, green_max, blue_max);
	printf("Max--> X: %.3f Y: %.3f Z: %.3f\n", x_max, y_max, z_max);
	printf("Min--> R: %.3f G: %.3f B: %.3f\n", red_min, green_min, blue_min);
	printf("Min--> X: %.3f Y: %.3f Z: %.3f\n", x_min, y_min, z_min);
	printf("Standard Deviation--> R: %.3f G: %.3f B: %.3f\n", sd_red, sd_green,
			sd_blue);
	printf("Standard Deviation--> X: %g Y: %g Z: %g\n", sd_x, sd_y, sd_z);

	return 0;
}

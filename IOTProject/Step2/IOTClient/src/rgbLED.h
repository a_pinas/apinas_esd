/*
 * rgbLED.h
 *
 *  Created on: May 7, 2021
 *      Author: rpi
 */

void initializeLED(void);

void gammaTable(void);

void updateLED(int r, int g, int b);

void stopLED(void);

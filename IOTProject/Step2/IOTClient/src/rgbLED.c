/*
 * rgbLED.c
 *
 *  Created on: May 7, 2021
 *      Author: rpi
 */
#include "rgbLED.h"
#include <pigpiod_if2.h>
#include <math.h>


#define PIN_R 17
#define PIN_G 18
#define PIN_B 27


#define PWM_FREQ 200

int pi;
uint8_t gammatable[256];

void initializeLED(void){
	/*pigpio initialization*/
	pi = pigpio_start(0, 0);	//Maybe start in the main program?

	/*GPIO PWM initialization*/
	set_PWM_dutycycle(pi, PIN_R, 0);
	set_PWM_dutycycle(pi, PIN_G, 0);
	set_PWM_dutycycle(pi, PIN_B, 0);

	set_PWM_frequency(pi, PIN_R, PWM_FREQ);
	set_PWM_frequency(pi, PIN_G, PWM_FREQ);
	set_PWM_frequency(pi, PIN_B, PWM_FREQ);

	gammaTable();
}

void gammaTable(void){

	  for (int i=0; i<256; i++) {
	    float x = i;
	    x /= 255;
	    x = pow(x, 2.5);
	    x *= 255;

	    gammatable[i] = x;
	  }
}

void stopLED(void){

	set_PWM_dutycycle(pi, PIN_R, 0);
	set_PWM_dutycycle(pi, PIN_G, 0);
	set_PWM_dutycycle(pi, PIN_B, 0);

	pigpio_stop(pi);

}

void updateLED(int r, int g, int b){
	set_PWM_dutycycle(pi, PIN_R, gammatable[(int)r]);
	set_PWM_dutycycle(pi, PIN_G, gammatable[(int)g]);
	set_PWM_dutycycle(pi, PIN_B, gammatable[(int)b]);
}

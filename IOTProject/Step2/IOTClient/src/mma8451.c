#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include "mma8451.h"


#define ADDR 0x1C				//MMA841 address

#define CTRL_REG1 0x2A			//Control Register 1
#define XYZ_DATA_CFG 0x0E		//XYZ data configuration register
#define OUT_X_MSB 0x01			//It's the first register we are going to read from.

float data_conversion(char MSB, char LSB);	//Converts the data

static int fd; 						//i2c file descriptor


int initializeMMA8451 (int i2c_fd){

	/*Getting file descriptor*/
	fd = i2c_fd;

	int w_len = 2;
	unsigned char write_bytes[w_len];
	unsigned char write_bytes2[w_len];

	printf("Initializing MMA8451...\n");

	struct i2c_rdwr_ioctl_data cfg_packets;
	struct i2c_msg cfg_messages[2];

	/* 4g scale, it has to be set before entering active mode*/
	write_bytes2[0] = XYZ_DATA_CFG;
	write_bytes2[1] = 0x01;
	cfg_messages[0].addr = ADDR;
	cfg_messages[0].flags = 0;
	cfg_messages[0].len = w_len;
	cfg_messages[0].buf = write_bytes2;
	
	/* Active mode*/
	write_bytes[0] = CTRL_REG1;
	write_bytes[1] = 0x01;
	cfg_messages[1].addr = ADDR;
	cfg_messages[1].flags = 0;
	cfg_messages[1].len = w_len;
	cfg_messages[1].buf = write_bytes;

	cfg_packets.msgs = cfg_messages;
	cfg_packets.nmsgs = 2;

	/*Sending configuration packet*/
	if( ioctl(fd, I2C_RDWR, &cfg_packets) < 0 ){
		printf("ioctl error\n");
		return -1;
	}

	printf("[RPI,MMA8451] initialized\n");
	return 0;
}

int readMMA8451(float *x_out, float *y_out, float *z_out){

	struct i2c_rdwr_ioctl_data packets;	//Packet to read from the sensor
	struct i2c_msg messages[2];
	int r_len = 6;
	unsigned char read_bytes_mma[r_len];

	unsigned char write_byte = OUT_X_MSB;
	messages[0].addr = ADDR;
	messages[0].flags = 0;
	messages[0].len = 1;
	messages[0].buf = &write_byte;

	messages[1].addr = ADDR;
	messages[1].flags = I2C_M_RD;
	messages[1].len = r_len;
	messages[1].buf = read_bytes_mma;

	packets.msgs = messages;
	packets.nmsgs = 2;

	float x, y ,z;


	if(ioctl(fd, I2C_RDWR, &packets) < 0){
		printf("[RPI,MMA8451]ioctl error");
		return -1;
	}

	x = data_conversion(read_bytes_mma[0], read_bytes_mma[1]);
	y = data_conversion(read_bytes_mma[2], read_bytes_mma[3]);
	z = data_conversion(read_bytes_mma[4], read_bytes_mma[5]);
	
	printf("X_Axis: %.3f\n", x);

	printf("Y_Axis: %.3f\n", y);

	printf("Z_Axis: %.3f\n", z);

	*x_out = x;
	*y_out = y;
	*z_out = z;

	return 0;
}

int stopMMA8451(void){

	int w_len = 2;
	unsigned char cfg[w_len];

	/*Turning off the sensor*/
    struct i2c_rdwr_ioctl_data final_packet;
	struct i2c_msg final_msg[1];
	cfg[0] = CTRL_REG1;
	cfg[1] = 0x00;
	final_msg[0].addr = ADDR;
	final_msg[0].flags = 0;
	final_msg[0].len = w_len;
	final_msg[0].buf = cfg;

	final_packet.msgs = final_msg;
	final_packet.nmsgs = 1;

	/*Sending the last packet*/
	if( ioctl(fd, I2C_RDWR, &final_packet) < 0 ){
		printf("[RPI,MMA8451]ioctl error\n");
		return -1;
	}
	return 0;
}



float data_conversion(char MSB, char LSB){

	int16_t data;
	data = (MSB << 8) | LSB;
	data = data >> 2;

	float out = (float) data / 2048;

	return out;
}


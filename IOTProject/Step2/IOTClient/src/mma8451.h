/*
 * mma8451.h
 *
 *  Created on: Apr 30, 2021
 *      Author: Alejandro
 */


int initializeMMA8451(int i2c_fd);

int readMMA8451(float *x, float *y, float *z);

int stopMMA8451(void);



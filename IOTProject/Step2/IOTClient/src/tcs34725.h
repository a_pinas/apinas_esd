/*
 * tcs34725.h
 *
 *  Created on: Apr 30, 2021
 *      Author: Alejandro
 */


int initializeTCS34725(int i2c_fd);

int readTCS34725(uint8_t *r_out, uint8_t *g_out, uint8_t *b_out);

int stopTCS34725(void);



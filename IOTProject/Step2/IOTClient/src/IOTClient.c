/*
 *  main.c
 *  IOTClient source
 *  Created on: Apr 26, 2021
 *      Author: rpi
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <stdio.h>
#include <stdbool.h>
#include "tcs34725.h"
#include "mma8451.h"
#include "rgbLED.h"

int initializeDevices(void);

#define MAXSIZE 1024

struct packet {
	int n_samples;
	int r[10];
	int g[10];
	int b[10];
	float x[10];
	float y[10];
	float z[10];
};

int sample_rate = 1; //1 second by default
int samples = 10;    //10 samples by default
bool run = true;

int main(int argc, char *argv[]) {

	if (argc < 3) {
		printf(
				"Usage: %s <host_address>  <port> [sample_rate_s] [samples_n] \n",
				argv[0]);
		printf(
				"[samples_n] is the number of samples to send to the server, range from 1 to 10 samples\n ");
		return 0;
	} else if (argc > 3) {
		sample_rate = atoi(argv[3]);
		samples = atoi(argv[4]);
	}

	uint16_t port = htons(atoi(argv[2]));

	printf("Starting communication with %s on port %s.\n", argv[1], argv[2]);
	printf("Sample rate: %d seconds, Number of samples: %d\n", sample_rate, samples);

	/*Socket configuration*/
	int socket_fd = socket(AF_INET, SOCK_DGRAM, 0); //AF_INET => IPv4 Internet protocols
	if (socket_fd < 0) {				//SOCK_DGRAM => Connectionless protocol
		printf("Error creating socket.\n");
		exit(1);
	} else {
		printf("Socket created.\n");
	}

	/*Initializing devices*/
	int i2c_fd = initializeDevices();

	/*Server information*/
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET; 					//IPv4
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);  	//Server IP
	server_addr.sin_port = port; 						//Server Port

	/*Client information*/
	struct sockaddr_in client_addr;
	client_addr.sin_family = AF_INET;			//IPv4
	client_addr.sin_addr.s_addr = INADDR_ANY;	//Any IP
	client_addr.sin_port = 0; 				//The system will choose the port

	char buffer[256];
	struct packet *out_packet = (struct packet*) buffer;
	uint8_t r, g, b;
	float x, y, z;

	/*Number of samples*/
	out_packet->n_samples = samples;

	/*loop*/
	while (run) {

		for(int i = 0; i < samples; i++){
			readMMA8451(&x, &y, &z);
			readTCS34725(&r, &g, &b);

			out_packet->r[i] = r;
			out_packet->g[i] = g;
			out_packet->b[i] = b;
			out_packet->x[i] = x;
			out_packet->y[i] = y;
			out_packet->z[i] = z;

			sleep(sample_rate);

		}


		/*Sending Message*/
		printf("Sending data to the server.\n");

		int status = sendto(socket_fd, out_packet, sizeof(struct packet), 0,
				(struct sockaddr*) &server_addr, sizeof(server_addr));
		if (status < 0) {
			printf("Error sending message.\n");
			exit(2);
		} else {
			printf("Message sent succesfully.\n");
		}

		/*Receiving message*/
		char in_buf[MAXSIZE];
		int client_size = sizeof(client_addr);

		status = recvfrom(socket_fd, in_buf, MAXSIZE, 0,
				(struct sockaddr*) &client_addr, &client_size);
		if (status < 0) {
			printf("Error receiving.\n");
			exit(3);
		} else {
			printf("Message received from the server.\n");
		}
		printf("[Server] %s\n", in_buf);

	}

	close(socket_fd);
	close(i2c_fd);
}

int initializeDevices(void) {

	int i2c_fd = open("/dev/i2c-1", O_RDWR);
	if (i2c_fd < 0) {
		printf("Error opening i2c-1");
		return -1;
	}

	initializeMMA8451(i2c_fd);
	initializeTCS34725(i2c_fd);
	//initializeLED();

	sleep(1);

	return i2c_fd;
}

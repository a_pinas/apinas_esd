/*
 * main.c
 *
 *  Created on: Mar 29, 2021
 *      Author: Alejandro Piñas, Alvaro Cañibano
 */

#include <math.h>
#include <pigpiod_if2.h>
#include <unistd.h>
#include <stdio.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>
#include <sys/types.h>
#include <stdbool.h>

#define ADDR 0x29

#define SAMPLE_RATE 2

#define PIN_R 17
#define PIN_G 18
#define PIN_B 27

#define PWM_FREQ 200

void SIGINT_handler(int sig); //SIGINT handler, the program will end when Ctrl+C is pressed
float getRGB(uint16_t colour, uint16_t clear); //Gets RGB value
void gammaTable(void);

uint16_t clear, red, green, blue;
float r, g, b;

bool run = true;

uint8_t gammatable[256];


int main (void){

	/*pigpio initialization*/
	int pi = pigpio_start(NULL, NULL);

	/*GPIO PWM initialization*/
	set_PWM_dutycycle(pi, PIN_R, 0);
	set_PWM_dutycycle(pi, PIN_G, 0);
	set_PWM_dutycycle(pi, PIN_B, 0);

	set_PWM_frequency(pi, PIN_R, PWM_FREQ);
	set_PWM_frequency(pi, PIN_G, PWM_FREQ);
	set_PWM_frequency(pi, PIN_B, PWM_FREQ);

	/*Initialization of the I2C*/
	printf("Initializing...\n");

	/*SIGINT Handler*/
	signal(SIGINT, SIGINT_handler);

	/*Opening file descriptor*/
	int fd = open("/dev/i2c-1", O_RDWR);
	if(fd < 0){
		printf("Error opening i2c-1");
		return -1;
	}
	if(ioctl(fd, I2C_SLAVE, ADDR) < 0){
		printf("ioctl error");
		return -1;
	}
	
	struct i2c_rdwr_ioctl_data cfg_packets;
	struct i2c_msg cfg_messages[4];

	int w_len = 2;

	/*ATIME Register configuration,	 */
	unsigned char RGBC_timing_cfg[w_len];

	RGBC_timing_cfg[0] = 0x81; //MSB selects the command register.
							   //5 LSB select the target register we are going to write to.

	RGBC_timing_cfg[1] = 0x00;	//Integration time
	cfg_messages[0].addr = ADDR;
	cfg_messages[0].flags = 0; // 0 => Write
	cfg_messages[0].len = w_len;
	cfg_messages[0].buf = RGBC_timing_cfg;

	/*CONTROL register configuration*/
	//Might not be needed, its set to 0x00 by default
	unsigned char CONTROL_cfg[w_len];
	CONTROL_cfg[0] = 0x8F; //5 LSB are the register address
	CONTROL_cfg[1] = 0x00; //Gain X1
	cfg_messages[1].addr = ADDR;
	cfg_messages[1].flags = 0; // 0 => Write
	cfg_messages[1].len = w_len;
	cfg_messages[1].buf = CONTROL_cfg;

	/*ENABLE register configuration*/
	unsigned char ENABLE_cfg[w_len];
	ENABLE_cfg[0] = 0x80; //5 LSB are the register address
	ENABLE_cfg[1] = 0x03; //RGBC enable, Power ON
	cfg_messages[2].addr = ADDR;
	cfg_messages[2].flags = 0; // 0 => Write
	cfg_messages[2].len = w_len;
	cfg_messages[2].buf = ENABLE_cfg;

	/*Setting CDATAL in the command register*/
	unsigned char write_byte = 0xB4; //Auto-increment. Reading starts at CDATAL
	cfg_messages[3].addr = ADDR;
	cfg_messages[3].flags = 0; // 0 => Write
	cfg_messages[3].len = 1;
	cfg_messages[3].buf = &write_byte;

	cfg_packets.msgs = cfg_messages;
	cfg_packets.nmsgs = 4;

	/*Sending configuration packet*/
	if( ioctl(fd, I2C_RDWR, &cfg_packets) < 0 ){
		printf("ioctl error\n");
		return -1;
	}

	/*Assambling data packet*/

	struct i2c_rdwr_ioctl_data packet;
	struct i2c_msg message[1];

	int r_len = 8;					//We are going to read 8 bytes
	unsigned char read_bytes[r_len];

	message[0].addr = ADDR;
	message[0].flags = I2C_M_RD;
	message[0].len = r_len;
	message[0].buf = read_bytes;

	packet.msgs = message;
	packet.nmsgs = 1;

	gammaTable(); 				//Builds gamma table

	sleep(1);
	printf("TCS35725 Initialized\n");

	while(run){

		if(ioctl(fd, I2C_RDWR, &packet) < 0){
			printf("ioctl error");
			return -1;
		}

		clear = (read_bytes[1] << 8) | read_bytes[0];
		red = (read_bytes[3] << 8) | read_bytes[2];
		green = (read_bytes[5] << 8) | read_bytes[4];
		blue = (read_bytes[7] << 8) | read_bytes[6];

		r = getRGB(red, clear);
		g = getRGB(green, clear);
		b = getRGB(blue, clear);

		printf("Raw Data->Clear: %d, Red: %d, Green: %d, Blue: %d\n", clear, red, green, blue);
		printf("R:%3.0f, G:%3.0f, B:%3.0f\n", r, g, b);

		set_PWM_dutycycle(pi, PIN_R, gammatable[(int)r]);
		set_PWM_dutycycle(pi, PIN_G, gammatable[(int)g]);
		set_PWM_dutycycle(pi, PIN_B, gammatable[(int)b]);


		sleep(SAMPLE_RATE);

	}

	printf("Turning off...\n");

	/*Turning off the sensor*/
    struct i2c_rdwr_ioctl_data final_packet;
	struct i2c_msg final_msg[1];
	ENABLE_cfg[0] = 0x80;
	ENABLE_cfg[1] = 0x00;
	final_msg[0].addr = ADDR;
	final_msg[0].flags = 0;
	final_msg[0].len = w_len;
	final_msg[0].buf = ENABLE_cfg;

	final_packet.msgs = final_msg;
	final_packet.nmsgs = 1;


	if( ioctl(fd, I2C_RDWR, &final_packet) < 0 ){
		printf("ioctl error\n");
		return -1;
	}

	close(fd);

	set_PWM_dutycycle(pi, PIN_R, 0);
	set_PWM_dutycycle(pi, PIN_G, 0);
	set_PWM_dutycycle(pi, PIN_B, 0);

	pigpio_stop(pi);

}

float getRGB (uint16_t colour, uint16_t clear){

	  float value;
	  if (clear == 0) {
	    return 0;
	  }

	  value = (float)colour / clear * 255.0;
	  return value;

}

void SIGINT_handler(int sig){

	printf("\nSIGINT Received\n");
	run = false;
}

void gammaTable(void){

	  for (int i=0; i<256; i++) {
	    float x = i;
	    x /= 255;
	    x = pow(x, 2.5);
	    x *= 255;

	    gammatable[i] = x;
	  }
}



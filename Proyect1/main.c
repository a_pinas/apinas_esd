#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>

#define ADDR 0x1C	//MMA841 address

#define CTRL_REG1 0x2A
#define XYZ_DATA_CFG 0x0E
#define OUT_X_MSB 0x01	//It's the first register we are going to read from.

void SIGINT_handler(int sig); //SIGINT handler, the program will end when Ctrl+C is pressed

float data_conversion(char MSB, char LSB);

bool run = true; 

int main(void){
	
	int16_t x_out, y_out, z_out;
	float x,y,z;
	int r_len = 6;
	char read_bytes[r_len];
	int w_len = 2;
	char write_bytes[w_len];
	char write_bytes2[w_len];

	/*SIGINT Handler*/
	signal(SIGINT, SIGINT_handler);

	printf("Initializing accelerometer...\n");
	/*Opening file descriptor*/
	int fd = open("/dev/i2c-1", O_RDWR);
	if(fd < 0){
		printf("Error trying to open i2c-1");
		return -1;	
	}

	if(ioctl(fd, I2C_SLAVE, ADDR) < 0){
		printf("ioctl error");
		return -1;
	}

	struct i2c_rdwr_ioctl_data cfg_packets;
	struct i2c_msg cfg_messages[2];

	/* 4g scale, it has to be set before entering active mode*/
	write_bytes2[0] = XYZ_DATA_CFG;
	write_bytes2[1] = 0x01;
	cfg_messages[0].addr = ADDR;
	cfg_messages[0].flags = 0;
	cfg_messages[0].len = w_len;
	cfg_messages[0].buf = write_bytes2;
	
	/* Active mode*/
	write_bytes[0] = CTRL_REG1;
	write_bytes[1] = 0x01;
	cfg_messages[1].addr = ADDR;
	cfg_messages[1].flags = 0;
	cfg_messages[1].len = w_len;
	cfg_messages[1].buf = write_bytes;

	cfg_packets.msgs = cfg_messages;
	cfg_packets.nmsgs = 2;

	if( ioctl(fd, I2C_RDWR, &cfg_packets) < 0 ){
		printf("ioctl error\n");
		return -1;
	}

	struct i2c_rdwr_ioctl_data packets;
	struct i2c_msg messages[2];
	
	/*Telling the sensor that we want to start reading 
	OUR_X_MSB register */

	unsigned char write_byte = OUT_X_MSB;
	messages[0].addr = ADDR;
	messages[0].flags = 0;
	messages[0].len = 1;
	messages[0].buf = &write_byte;

	messages[1].addr = ADDR;
	messages[1].flags = I2C_M_RD;
	messages[1].len = r_len;
	messages[1].buf = read_bytes;

	packets.msgs = messages;
	packets.nmsgs = 2;

	sleep(1);
	printf("Accelerometer initialized\n");
	
	while(run){

		if(ioctl(fd, I2C_RDWR, &packets) < 0){
			printf("ioctl error");
			return -1;
		}

		x = data_conversion(read_bytes[0], read_bytes[1]);
		y = data_conversion(read_bytes[2], read_bytes[3]);
		z = data_conversion(read_bytes[4], read_bytes[5]);

		printf("X_Axis: %.3f\n", x);
		printf("Y_Axis: %.3f\n", y);
		printf("Z_Axis: %.3f\n", z);
		sleep(3);
		
	}
 	
	close(fd);
}

float data_conversion(char MSB, char LSB){

	int16_t data;
	data = (MSB << 8) | LSB;
	data = data >> 2;

	float out = (float) data / 2048;

	return out;
}

void SIGINT_handler(int sig){

	printf("\nSIGINT Received\n");
	run = false;

}